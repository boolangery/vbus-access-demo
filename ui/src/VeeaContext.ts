import React from "react";
import AccessApi from "vbus-access-api/dist/AccessApi";

const VeeaContext = React.createContext<AccessApi>(null);

export default VeeaContext
