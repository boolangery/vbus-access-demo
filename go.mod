module info

go 1.13

require (
	bitbucket.org/vbus/vbus.go v1.0.3-0.20200423100303-7cdde230e36d
	github.com/sirupsen/logrus v1.4.2
	github.com/zserge/webview v0.0.0-20200410160118-50e4bcc420ab // indirect
)


replace bitbucket.org/vbus/vbus.go => ../vbus.go
